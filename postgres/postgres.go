package postgres

import (
	"context"
	"fmt"
	"parser/Parser/model"
	"sync"

	"github.com/jackc/pgx/v5"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type VacancyStorage struct {
	db                 *pgx.Conn
	autoIncrementCount int
	*sync.Mutex
}

func NewVacancyStorage(db *pgx.Conn) *VacancyStorage {
	return &VacancyStorage{
		db:                 db,
		autoIncrementCount: 1,
		Mutex:              &sync.Mutex{},
	}
}

func NewConn(database_url string) (*pgx.Conn, error) {
	conn, err := pgx.Connect(context.Background(), database_url)

	if err != nil {
		return nil, fmt.Errorf("Unable to connect to database: %v\n", err)
	}

	return conn, nil
}

func (vs *VacancyStorage) CreateVacancy(vacancy model.Vacancy) error {
	vacancy.ID = primitive.NewObjectID()

	_, err := vs.db.Exec(context.Background(), `
		INSERT INTO vacancies (
			id, context, type, date_posted, title, description,
			identifier_type, identifier_name, identifier_value,
			hiring_organization_type, hiring_organization_name, hiring_organization_same_as,
			job_location_place_type, job_location_address_type, job_location_street_address,
			job_location_address_locality, job_location_address_country_type, job_location_address_country_name,
			employment
		)
		VALUES (
			$1, $2, $3, $4, $5, $6,
			$7, $8, $9,
			$10, $11, $12,
			$13, $14, $15,
			$16, $17, $18,
			$19
		)
	`, vacancy.ID, vacancy.Context, vacancy.Type, vacancy.DatePosted, vacancy.Title, vacancy.Description,
		vacancy.Identifier.Type, vacancy.Identifier.Name, vacancy.Identifier.Value,
		vacancy.HiringOrg.Type, vacancy.HiringOrg.Name, vacancy.HiringOrg.SameAs,
		vacancy.JobLocation.PlaceType, vacancy.JobLocation.Address.AddressType, vacancy.JobLocation.Address.StreetAddress,
		vacancy.JobLocation.Address.AddressLocality, vacancy.JobLocation.Address.AddressCountry.CountryType, vacancy.JobLocation.Address.AddressCountry.CountryName,
		vacancy.Employment)

	if err != nil {
		return fmt.Errorf("Unable to create vacancy: %v", err)
	}

	return nil
}

func (vs *VacancyStorage) GetAllVacancies() ([]model.Vacancy, error) {
	rows, err := vs.db.Query(context.Background(), "SELECT * FROM vacancies")
	if err != nil {
		return nil, fmt.Errorf("failed to query vacancies: %v", err)
	}
	defer rows.Close()

	var vacancies []model.Vacancy

	for rows.Next() {
		var vacancy model.Vacancy

		err := rows.Scan(
			&vacancy.ID,
			&vacancy.Context,
			&vacancy.Type,
			&vacancy.DatePosted,
			&vacancy.Title,
			&vacancy.Description,
			&vacancy.Identifier.Type,
			&vacancy.Identifier.Name,
			&vacancy.Identifier.Value,
			&vacancy.HiringOrg.Type,
			&vacancy.HiringOrg.Name,
			&vacancy.HiringOrg.SameAs,
			&vacancy.JobLocation.PlaceType,
			&vacancy.JobLocation.Address.AddressType,
			&vacancy.JobLocation.Address.StreetAddress,
			&vacancy.JobLocation.Address.AddressLocality,
			&vacancy.JobLocation.Address.AddressCountry.CountryType,
			&vacancy.JobLocation.Address.AddressCountry.CountryName,
		)
		if err != nil {
			return nil, fmt.Errorf("failed to scan vacancy: %v", err)
		}

		vacancies = append(vacancies, vacancy)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("error while iterating over rows: %v", err)
	}

	return vacancies, nil
}

func (vs *VacancyStorage) GetVacancyById(id primitive.ObjectID) (*model.Vacancy, error) {
	vacancy := &model.Vacancy{}

	err := vs.db.QueryRow(context.Background(), `
		SELECT id, context, type, date_posted, title, description,
			identifier_type, identifier_name, identifier_value,
			hiring_organization_type, hiring_organization_name, hiring_organization_same_as,
			job_location_place_type, job_location_address_type, job_location_street_address,
			job_location_address_locality, job_location_address_country_type, job_location_address_country_name,
			employment
		FROM vacancies
		WHERE id = $1
	`, id).Scan(
		&vacancy.ID, &vacancy.Context, &vacancy.Type, &vacancy.DatePosted, &vacancy.Title, &vacancy.Description,
		&vacancy.Identifier.Type, &vacancy.Identifier.Name, &vacancy.Identifier.Value,
		&vacancy.HiringOrg.Type, &vacancy.HiringOrg.Name, &vacancy.HiringOrg.SameAs,
		&vacancy.JobLocation.PlaceType, &vacancy.JobLocation.Address.AddressType, &vacancy.JobLocation.Address.StreetAddress,
		&vacancy.JobLocation.Address.AddressLocality, &vacancy.JobLocation.Address.AddressCountry.CountryType, &vacancy.JobLocation.Address.AddressCountry.CountryName,
		&vacancy.Employment)

	if err != nil {
		if err == pgx.ErrNoRows {
			return nil, fmt.Errorf("Vacancy not found")
		}
		return nil, fmt.Errorf("Unable to get vacancy: %v", err)
	}
	return vacancy, nil
}

func (vs *VacancyStorage) UpdateVacancyById(id primitive.ObjectID, vacancy model.Vacancy) error {
	_, err := vs.db.Exec(context.Background(), `
		UPDATE vacancies
		SET
			context = $1, type = $2, date_posted = $3, title = $4, description = $5,
			identifier_type = $6, identifier_name = $7, identifier_value = $8,
			hiring_organization_type = $9, hiring_organization_name = $10, hiring_organization_same_as = $11,
			job_location_place_type = $12, job_location_address_type = $13, job_location_street_address = $14,
			job_location_address_locality = $15, job_location_address_country_type = $16, job_location_address_country_name = $17,
			employment = $18
		WHERE id = $19
	`, vacancy.Context, vacancy.Type, vacancy.DatePosted, vacancy.Title, vacancy.Description,
		vacancy.Identifier.Type, vacancy.Identifier.Name, vacancy.Identifier.Value,
		vacancy.HiringOrg.Type, vacancy.HiringOrg.Name, vacancy.HiringOrg.SameAs,
		vacancy.JobLocation.PlaceType, vacancy.JobLocation.Address.AddressType, vacancy.JobLocation.Address.StreetAddress,
		vacancy.JobLocation.Address.AddressLocality, vacancy.JobLocation.Address.AddressCountry.CountryType, vacancy.JobLocation.Address.AddressCountry.CountryName,
		vacancy.Employment, id)

	if err != nil {
		return fmt.Errorf("Unable to update vacancy: %v", err)
	}

	return nil
}

func (vs *VacancyStorage) DeleteVacancyById(id primitive.ObjectID) error {
	_, err := vs.db.Exec(context.Background(), `
		DELETE FROM vacancies
		WHERE id = $1
	`, id)

	if err != nil {
		return fmt.Errorf("Unable to delete vacancy: %v", err)
	}

	return nil
}
