FROM golang:1.19-alpine

WORKDIR /app

COPY go.sum ./

COPY go.mod ./

COPY ./Parser ./Parser

RUN go mod download

RUN ls

RUN go build -o ./Parser/main ./Parser

WORKDIR /app/Parser

CMD ["./main"]