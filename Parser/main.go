package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"parser/Parser/handlers"
	"parser/Parser/model"
	"parser/postgres"
	"text/template"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

const (
	DATABASE_URL = "postgres://postgres:postgres@localhost:5432/vacancies?sslmode=verify-ca"
)

func main() {

	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	// mongoDB, err := mongo.NewMongoDB("MyCluster", "vacancies")
	// if err != nil {
	// 	fmt.Println(err)
	// }

	dbConn, err := postgres.NewConn(DATABASE_URL)
	if err != nil {
		log.Fatalf("Unable to connect to PostgreSQL: %v", err)
	}
	defer dbConn.Close(context.Background())

	// Создание экземпляра VacancyStorage
	vacancyStorage := postgres.NewVacancyStorage(dbConn)

	// Создание экземпляра DBController
	dbController := handlers.NewDBController(vacancyStorage)

	vacancyController := handlers.NewVacancyController("./repo.json")
	r.Post("/search", vacancyController.SerachVacancy)
	r.Post("/delete", vacancyController.DeleteVacancyById)
	r.Post("/get", vacancyController.GetVacancyById)
	r.Get("/list", vacancyController.GetList)

	r.Get("/vacancies", dbController.GetAllVacanciesHandler)
	r.Post("/vacancies", dbController.CreateVacancyHandler)
	r.Get("/vacancies/{id}", dbController.GetVacancyByIDHandler)
	r.Put("/vacancies/{id}", dbController.UpdateVacancyByIDHandler)
	r.Delete("/vacancies/{id}", dbController.DeleteVacancyByIDHandler)

	//SwaggerUI
	r.Get("/swagger", swaggerUI)
	// r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
	// 	http.StripPrefix("/public/", http.FileServer(http.Dir("./"))).ServeHTTP(w, r)
	// })
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	searchURL := "http://localhost:8080/search"

	resp, err := http.Post(searchURL, "application/json", nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return
	}

	var vacancies []model.Vacancy
	err = json.Unmarshal(body, &vacancies)
	if err != nil {
		fmt.Println(err)
		return
	}

	// for _, vacancy := range vacancies {
	// 	if err := mongoDB.CreateVacancy(vacancy); err != nil {
	// 		fmt.Println("Can't create vacancy in mongo db")
	// 	}
	// }

	for _, vacansy := range vacancies {
		if err := vacancyStorage.CreateVacancy(vacansy); err != nil {
			fmt.Println("Can't create vacancy in postgres")
		}
	}

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
