package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"parser/Parser/logic"
	"parser/Parser/storage"

	"github.com/tebeka/selenium"
	"github.com/tebeka/selenium/chrome"
)

type VacancyController struct {
	storage storage.Storage
}

func NewVacancyController(filepath string) *VacancyController {
	return &VacancyController{storage: *storage.NewStorage(filepath)}
}

func (v *VacancyController) SerachVacancy(w http.ResponseWriter, r *http.Request) {
	maxTries := 15
	// прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}

	// добавляем в конфигурацию драйвера настройки для chrome
	chrCaps := chrome.Capabilities{
		W3C: true,
	}
	caps.AddChrome(chrCaps)

	// переменная нашего веб драйвера
	var wd selenium.WebDriver
	var err error
	// прописываем адрес нашего драйвера
	urlPrefix := selenium.DefaultURLPrefix
	// немного костылей чтобы драйвер не падал
	for i := 0; i < maxTries; {
		wd, err = selenium.NewRemote(caps, urlPrefix)
		if err != nil {
			i++
			continue
		}
		break
	}

	var query map[string]string
	if err := json.NewDecoder(r.Body).Decode(&query); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	Vacancies, err := logic.VacanciesLogic(&wd, query["query"])
	if err != nil {
		fmt.Println(err)
	}

	for _, vacancy := range Vacancies {
		if err := v.storage.Create(vacancy); err != nil {
			fmt.Println(err)
		}
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8

	err = json.NewEncoder(w).Encode(Vacancies) // записываем результат Pet json в http.ResponseWriter
	if err != nil {                            // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = wd.Quit()
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (v *VacancyController) DeleteVacancyById(w http.ResponseWriter, r *http.Request) {
	var query map[string]int
	if err := json.NewDecoder(r.Body).Decode(&query); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := v.storage.Delete(query["id"]); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err := json.NewEncoder(w).Encode("Deleting is successful"); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (v *VacancyController) GetVacancyById(w http.ResponseWriter, r *http.Request) {
	var query map[string]int
	if err := json.NewDecoder(r.Body).Decode(&query); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	Vacancy, err := v.storage.GetById(query["id"])
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(Vacancy)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (v *VacancyController) GetList(w http.ResponseWriter, r *http.Request) {
	Vacancies, err := v.storage.GetList()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = json.NewEncoder(w).Encode(Vacancies)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
