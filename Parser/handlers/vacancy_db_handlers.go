package handlers

import (
	"encoding/json"
	"net/http"
	"parser/Parser/model"
	"parser/Parser/mongo"

	"github.com/go-chi/chi/v5"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DBController struct {
	Db mongo.Storager
}

func NewDBController(db mongo.Storager) *DBController {
	return &DBController{Db: db}
}

func (dc *DBController) GetAllVacanciesHandler(w http.ResponseWriter, r *http.Request) {
	vacancies, err := dc.Db.GetAllVacancies()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(vacancies)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (dc *DBController) CreateVacancyHandler(w http.ResponseWriter, r *http.Request) {
	var vacancy model.Vacancy
	err := json.NewDecoder(r.Body).Decode(&vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = dc.Db.CreateVacancy(vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (dc *DBController) GetVacancyByIDHandler(w http.ResponseWriter, r *http.Request) {
	vacancyID := chi.URLParam(r, "id")
	id, err := primitive.ObjectIDFromHex(vacancyID)
	if err != nil {
		http.Error(w, "Invalid vacancy ID", http.StatusBadRequest)
		return
	}

	vacancy, err := dc.Db.GetVacancyById(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (dc *DBController) UpdateVacancyByIDHandler(w http.ResponseWriter, r *http.Request) {
	vacancyID := chi.URLParam(r, "id")
	id, err := primitive.ObjectIDFromHex(vacancyID)
	if err != nil {
		http.Error(w, "Invalid vacancy ID", http.StatusBadRequest)
		return
	}

	var vacancy model.Vacancy
	err = json.NewDecoder(r.Body).Decode(&vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = dc.Db.UpdateVacancyById(id, vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (dc *DBController) DeleteVacancyByIDHandler(w http.ResponseWriter, r *http.Request) {
	vacancyID := chi.URLParam(r, "id")
	id, err := primitive.ObjectIDFromHex(vacancyID)
	if err != nil {
		http.Error(w, "Invalid vacancy ID", http.StatusBadRequest)
		return
	}

	err = dc.Db.DeleteVacancyById(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
