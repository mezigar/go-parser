package storage

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"

	"parser/Parser/model"
)

type Storage struct {
	filepath string
	repo     map[int]model.Vacancy
}

type Storager interface {
	Create(dto model.Vacancy) error
	GetById(id int) (model.Vacancy, error)
	GetList() ([]model.Vacancy, error)
	Delete(id int) error
	LoadData() (map[int]model.Vacancy, error)
}

func NewStorage(filepath string) *Storage {
	return &Storage{
		filepath: filepath,
		repo:     make(map[int]model.Vacancy),
	}
}

func (s *Storage) saveData() error {
	data, err := json.MarshalIndent(s.repo, "", "	")
	if err != nil {
		fmt.Println(err)
		return err
	}
	f, err := os.OpenFile(s.filepath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer f.Close()
	if err = f.Truncate(0); err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = f.Seek(0, 0); err != nil {
		fmt.Println(err)
		return err
	}
	if _, err = f.Write(data); err != nil {
		fmt.Println(err)
		return err
	}
	return nil

}

func (s *Storage) LoadData() (map[int]model.Vacancy, error) {
	f, err := os.OpenFile(s.filepath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	defer f.Close()

	var m map[int]model.Vacancy

	data, err := io.ReadAll(f)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	if err = json.Unmarshal(data, &m); err != nil {
		fmt.Println(err)
		return nil, err
	}

	return m, nil

}

func (s *Storage) Create(dto model.Vacancy) error {
	id, err := strconv.Atoi(dto.Identifier.Value)
	fmt.Println(id)
	if err != nil {
		fmt.Println(err)
		return err
	}
	s.repo[id] = dto
	if err = s.saveData(); err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (s *Storage) GetById(id int) (model.Vacancy, error) {
	var vacancy model.Vacancy
	if v, ok := s.repo[id]; !ok {
		return vacancy, fmt.Errorf("No vacancy with such id")
	} else {
		vacancy = v
		return vacancy, nil
	}
}

func (s *Storage) GetList() ([]model.Vacancy, error) {
	Vacancies := make([]model.Vacancy, 0, 200)

	for _, vacancy := range s.repo {
		Vacancies = append(Vacancies, vacancy)
	}

	if len(Vacancies) == 0 {
		return Vacancies, fmt.Errorf("No vacancies in storage")
	} else {
		return Vacancies, nil
	}
}

func (s *Storage) Delete(id int) error {
	if _, ok := s.repo[id]; !ok {
		return fmt.Errorf("No vacancy with such id")
	} else {
		delete(s.repo, id)
		if err := s.saveData(); err != nil {
			fmt.Println(err)
			return err
		}
		return nil
	}
}
