package logic

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strconv"

	"parser/Parser/model"

	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
)

const (
	maxTries         = 15
	habrLink         = "https://career.habr.com"
	vacanciesPerPage = 25
)

func VacanciesLogic(wd *selenium.WebDriver, query string) ([]model.Vacancy, error) {
	var vacancyCounter int
	for j := 1; j < maxTries; {
		vacancyCount, err := countVacancies(wd, query)
		vacancyCounter = vacancyCount
		if err != nil {
			j++
			continue
		}
		break
	}

	isDividable := (vacancyCounter % vacanciesPerPage) == 0
	var ExtraPage int
	if !isDividable {
		ExtraPage = 1
	}
	pages := vacancyCounter/vacanciesPerPage + ExtraPage

	links := make([]string, 0, 200)

	for i := 1; i <= pages; i++ {
		for j := 0; j < 10*maxTries; {

			linksPerPage, err := collectLinks(wd, i, query)
			if err != nil {
				j++
				continue
			}
			links = append(links, linksPerPage...)
			break
		}

	}

	Vacancies := make([]model.Vacancy, 0, 200)

	for _, link := range links {
		for j := 1; j < maxTries; {
			data, err := getDataFromLink(link)
			if err != nil {
				j++
				continue
			}

			var Vacancy model.Vacancy

			if err := json.Unmarshal([]byte(data), &Vacancy); err != nil {
				fmt.Println(err)
			}
			fmt.Println(Vacancy)
			Vacancies = append(Vacancies, Vacancy)
			break
		}

	}
	return Vacancies, nil
}

func countVacancies(wd *selenium.WebDriver, query string) (int, error) {

	page := 1 // номер страницы
	if err := (*wd).Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query)); err != nil {
		fmt.Println(err)
		return 0, err
	}
	elem, err := (*wd).FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		fmt.Println(err)
		return 0, err
	}

	vacancyCountRaw, err := elem.Text()
	if err != nil {
		fmt.Println(err)
		return 0, err
	}

	re, _ := regexp.Compile(`\d+`)
	rawCnt := re.FindString(vacancyCountRaw)

	vacancyCount, err := strconv.Atoi(rawCnt)
	if err != nil {
		fmt.Println(err)
		return 0, err
	}
	return vacancyCount, nil
}

func collectLinks(wd *selenium.WebDriver, page int, query string) ([]string, error) {
	if err := (*wd).Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query)); err != nil {
		fmt.Println(err)
		return []string{}, err
	}

	elems, err := (*wd).FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		fmt.Println(err)
		return []string{}, err
	}
	var links []string

	for i := range elems {
		var link string
		link, err := elems[i].GetAttribute("href")
		if err != nil {
			continue
		}
		links = append(links, habrLink+link)
	}

	return links, nil
}

func getDataFromLink(link string) (string, error) {
	resp, err := http.Get(link)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	var doc *goquery.Document
	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil && doc != nil {
		fmt.Println(err)
		return "", err
	}
	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		return "", fmt.Errorf("habr vacancy nodes not found")
	}

	ss := dd.First().Text()
	return ss, nil
}
